# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.0.4 (2021-05-24)

### [1.0.3](https://gitlab.com/megabyte-space/npm/buildr/compare/v1.0.2...v1.0.3) (2021-02-28)

### [1.0.2](https://gitlab.com/megabyte-space/npm/buildr/compare/v1.0.1...v1.0.2) (2021-02-28)

### 1.0.1 (2020-01-23)
