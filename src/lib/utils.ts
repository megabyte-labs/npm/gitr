import { exec as EXEC } from 'child_process'
import path from 'path'
import { promisify } from 'util'

import { Logger } from './log'

const exec = promisify(EXEC)

/**
 * Set upstream of the repository
 *
 * @param {string} url Remote url
 * @param {string} name Remote name. Default to "origin"
 * @returns {Promise<true>} Returns true if remote remote repository set. Otherwise returns false
 */
async function setRepoRemote(url: string, name: string = 'origin'): Promise<true> {
  Logger.info(`Setting remote ${name} to ${url}`)
  try {
    await exec(`git remote add ${name} ${url}`)
  } catch {
    await exec(`git remote set-url ${name} ${url}`)
  }

  return true
}

/**
 * Get name of the current branch.
 *
 * @returns {Promise<string>} Returns current branch name
 */
async function getCurrentBranch(): Promise<string> {
  const { stdout } = await exec('git branch --show-current')

  return stdout
}

/**
 * Initialize a git repository inside the current directory
 *
 * @returns {Promise<boolean>} Returns operation status
 */
async function initGitRepo(): Promise<boolean> {
  Logger.info('Initializing current directory as a git repository')
  const { stdout } = await exec('git init')

  return stdout && stdout.toLowerCase().includes('initialized') ? true : false
}

/**
 * Is currently inside a git repository?
 *
 * @returns {Promise<boolean>} Returns true if current working directory is a git repository. Otherwise false
 */
async function isInsideGitRepo(): Promise<boolean> {
  // is current directory git repository?
  try {
    const { stdout } = await exec('git rev-parse --is-inside-work-tree')

    return stdout.trim().toLowerCase() === 'true'
  } catch (error) {
    if (error.stderr.includes('not a git repository')) {
      return false
    }

    throw error
  }
}

/**
 * Commit all changes
 *
 * @param {string} configFile Configuration file path to exclude
 * @param {string} message Initial commit message. Default to "Initial commit"
 * @returns {Promise<boolean>} Returns true if commit success. Otherwise false
 */
async function commitAll(configFile: string, message: string = 'Initial commit'): Promise<boolean> {
  Logger.info(`Committing all changes to repository excluding ${configFile}`)

  // has changes to commit?
  const { stdout } = await exec('git status --porcelain')
  if (stdout.trim() === '') {
    Logger.warn('Nothing to commit')

    return false
  }

  await exec('git add --all')
  // exclude config file from staged because it contains secrets
  await exec(`git reset -- ${configFile}`)

  const { stdout: std } = await exec('git diff --cached --name-only')
  if (!std.trim()) {
    Logger.warn('Nothing to commit')

    return false
  }

  const { stderr } = await exec(`git commit -am "${message}"`)

  return !stderr
}

/**
 * Push working directory to remote
 *
 * @param {string} branch Branch name. Default "master"
 * @param {string} name Origin name. Default "origin"
 * @returns {Promise<boolean>} Returns true if git push success otherwise returns false
 */
async function pushToRemote(branch: string = 'master', name: string = 'origin'): Promise<boolean> {
  Logger.info(`Pushing changes to remote ${name}`)
  const { stderr } = await exec(`git push ${name} -u ${branch}`)

  return !stderr
}

/**
 * Commit current directory to the GitLab remote project
 *
 * Current directory is the directory where the configuration file is
 *
 * @param {string} configFile Configuration file path
 * @param {string} remoteUrl Remote URL of the remote repository
 * @returns {Promise<boolean>} Returns true if upload success, otherwise returns false
 */
// eslint-disable-next-line unicorn/prevent-abbreviations
export async function uploadCurrentDir(configFile: string, remoteUrl: string): Promise<boolean> {
  // store current working directory
  const originalWD = process.cwd()
  const directory = path.dirname(configFile)
  try {
    // change current working directory
    Logger.info(`Changing current working directory to ${directory}`)
    process.chdir(directory)

    // is inside git repository?
    if (!(await isInsideGitRepo())) {
      // init git repository
      await initGitRepo()
    }
    const branch = await getCurrentBranch()

    // set remote to gitlab.com
    await setRepoRemote(remoteUrl)

    // commit all inside the current directory
    const committed = await commitAll(configFile)

    // push to remote
    if (committed) {
      await pushToRemote(branch)
    }

    return true
  } catch (error) {
    Logger.error('Failed to upload current directory to GitLab')
    Logger.error(error)

    return false
  } finally {
    Logger.info(`Changing current working directory to ${originalWD}`)
    process.chdir(originalWD)
  }
}
