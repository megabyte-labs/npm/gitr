import { Octokit } from '@octokit/rest'

import { GitrOptions } from '../models/gitr-options.model'
import { GitHubPathData, IGitHubURLs, Repository } from '../models/object-interfaces'

import { Logger } from './log'

/**
 * GitHub helper
 */
export class GitHub {
  constructor(token: string) {
    this.api = new Octokit({ auth: token })
  }
  private readonly api

  /**
   * Parse GitHub path given in the configuration.
   *
   * Path must consist of either username/repository or organization/repository form.
   * It will throw errors if it's not in acceptable form.
   *
   * @param {string} path GitHub path
   * @returns {GitHubPathData} Parsed data from the given path
   * @throws Errors if parsing fails
   */
  public static parsePath(path: string): GitHubPathData {
    const elements = path.trim().split('/')
    const minPathElements = 1
    if (elements.length < minPathElements) {
      throw new Error('path must contain username/organization with repository slug or just repository slug')
    }
    const maxPathElements = 2
    if (elements.length > maxPathElements) {
      throw new Error('path must contain only username/organization and repository slug')
    }
    // eslint-disable-next-line functional/immutable-data
    const repo = elements.pop()
    // is empty string?
    if (!repo) {
      throw new Error('path contains empty repository slug')
    }

    // eslint-disable-next-line functional/immutable-data
    const owner = elements.pop()
    if (!owner) {
      throw new Error('path contains empty owner')
    }

    return {
      owner,
      path,
      repo,
    }
  }

  /**
   * Get GitHub url without authentication data
   *
   * @param {string} repo Repository path
   * @returns {string} Returns GitHub url
   */
  public static getGitHubMirrorURLWithoutAuth(repo: string): string {
    return `github.com/${repo}.git`
  }

  /**
   * Get GitHub Mirror URL
   *
   * @param {string} owner Github repository owner
   * @param {string} token Personal access token
   * @param {string} repo Github repository full name
   * @returns {IGitHubURLs} Returns Github URL data
   */
  public static getGitHubMirrorURLs(owner: string, token: string, repo: string): IGitHubURLs {
    const url = GitHub.getGitHubMirrorURLWithoutAuth(repo)

    return {
      withAuth: `https://${owner}:${token}@${url}`,
      withoutAuth: url,
    }
  }

  /**
   * Is given string an authenticated user's login username?
   *
   * @param {string} username username string
   * @returns {Promise<boolean>} true if it's same as the username or false otherwise
   */
  public async isUsername(username: string): Promise<boolean> {
    try {
      const {
        data: { login },
      } = await this.api.rest.users.getAuthenticated()

      return username === login
    } catch (error) {
      Logger.error(error)
    }

    return false
  }

  /**
   * Create GitHub project from the given options
   *
   * It will return project details if already exists
   *
   * @param {GitHubPathData} data project data
   * @param {GitrOptions} config configurations
   * @returns {Promise<Repository>} Returns GitHub repository data
   */
  public async createProject(data: GitHubPathData, config: GitrOptions): Promise<Repository> {
    Logger.info(`GitHub: Creating repository "${config.githubPath}"`)
    // is repository exist?
    let { owner } = data
    const { repo } = data
    const username = await this.getUsername()
    if (!owner) {
      owner = username
    }
    const project = await this.isProjectExists(owner, repo)

    if (config.wiki && !config.public) {
      Logger.warn('Wikis are not available in private repositories with GitHub Free and GitHub Free for organizations')
    }

    if (project) {
      Logger.warn(`GitHub: Repository "${config.githubPath}" already exists`)
      if (config.overwrite) {
        await this.api.rest.repos.update({
          description: config.description,
          has_wiki: config.wiki,
          owner,
          private: !config.public,
          repo: repo,
        })
        Logger.log(`GitHub: Repository "${config.githubPath}" updated`)
      }

      return project
    }

    // need to create user repository?
    if (!data.owner || data.owner === username) {
      const responseUser = await this.api.rest.repos.createForAuthenticatedUser({
        description: config.description,
        has_wiki: config.wiki,
        name: repo,
        private: !config.public,
      })
      Logger.info(`GitHub: "${config.githubPath}" repository created`)

      return responseUser.data
    }

    // try creating organization repository by assuming owner is a organization
    // path owned by current user
    const responseOrg = await this.api.rest.repos.createInOrg({
      description: config.description,
      has_wiki: config.wiki,
      name: repo,
      org: data.owner,
      private: !config.public,
    })
    Logger.info(`GitHub: "${config.githubPath}" repository created`)

    return responseOrg.data
  }

  /**
   * Get authenticated user's username
   *
   * @returns {Promise<string>} username
   */
  private async getUsername(): Promise<string> {
    const {
      data: { login },
    } = await this.api.rest.users.getAuthenticated()

    return login
  }

  /**
   * Is project already exist?
   *
   * @param {string} owner GitHub owner, either username or organization name
   * @param {string} repo GitHub full path
   * @returns {Promise<Repository|false>} Returns repository data if exists, otherwise returns false
   */
  private async isProjectExists(owner: string, repo: string): Promise<Repository | false> {
    try {
      const { data } = await this.api.rest.repos.get({ owner, repo })

      return data
    } catch (error) {
      if (error.status.toString() === '404') {
        return false
      }

      throw error
    }
  }
}
