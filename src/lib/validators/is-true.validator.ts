import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator'

/**
 * Is True validation decorator
 *
 * @param {any} property Property to validate
 * @param {ValidationOptions} validationOptions Validation options
 * @returns {Function} Returns callback
 */
export const IsTrue = (property: unknown, validationOptions?: ValidationOptions): Function => (
  object: object,
  propertyName: string
): void => {
  registerDecorator({
    constraints: [property],
    name: 'IsTrue',
    options: validationOptions,
    propertyName,
    target: object.constructor,
    validator: {
      validate(value: unknown, arguments_: ValidationArguments): boolean {
        if (typeof arguments_.constraints[0] === 'function') {
          // eslint-disable-next-line unicorn/no-null
          return !!Reflect.apply(arguments_.constraints[0], null, [value])
        }

        return false
      },
    },
  })
}
