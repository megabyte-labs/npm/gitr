/**
 * GitLab path data
 */
export type GitLabPathData = {
  readonly repoSlug: string
  readonly groups: readonly string[]

  readonly groupPath?: string
}
