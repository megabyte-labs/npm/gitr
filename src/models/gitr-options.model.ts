import { IsBoolean, IsOptional, IsString } from 'class-validator'

/**
 * The master data model that includes
 */
export class GitrOptions {
  @IsString()
  public readonly description: string

  @IsString()
  public readonly gitlabToken: string

  @IsString()
  public readonly githubToken: string

  @IsString()
  public readonly githubPath: string

  @IsString()
  public readonly gitlabPath: string

  @IsString()
  public readonly gitlabTitle: string

  @IsBoolean()
  public readonly mirror: boolean

  @IsBoolean()
  @IsOptional()
  public readonly public: boolean

  @IsBoolean()
  @IsOptional()
  public readonly wiki: boolean

  @IsBoolean()
  public readonly uploadCurrentDir?: boolean

  @IsBoolean()
  public readonly overwrite?: boolean

  constructor(options: GitrOptions) {
    this.description = options.description
    this.gitlabToken = options.gitlabToken
    this.githubToken = options.githubToken
    this.gitlabPath = options.gitlabPath
    this.githubPath = options.githubPath
    this.gitlabTitle = options.gitlabTitle
    this.mirror = options.mirror
    this.public = options.public
    this.wiki = options.wiki
    this.uploadCurrentDir = options.uploadCurrentDir === undefined ? false : options.uploadCurrentDir
    this.overwrite = options.overwrite === undefined ? true : options.overwrite
  }
}
