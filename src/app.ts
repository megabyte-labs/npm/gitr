import { validate } from 'class-validator'

import { GitHub } from './lib/github'
import { GitLab } from './lib/gitlab'
import { Logger } from './lib/log'
import { uploadCurrentDir as uploadCurrentDirectory } from './lib/utils'
import { GitrOptions } from './models/gitr-options.model'

/**
 * This class exposes the base API for Buildr. In general, you should be
 * using this as the entry point.
 */
export class Gitr {
  /**
   * This method runs through all the tasks that are provided by Buildr.
   *
   * @param {GitrOptions} data Options in the form of [[GitrOptions]]
   * @param {string} configFile Configuration file path
   * @returns {Promise<void>} A void Promise
   */
  public static async run(data: GitrOptions, configFile: string): Promise<void> {
    const config = new GitrOptions(data)
    const errors = await validate(config)

    // are there validation errors?
    if (errors.length > 0) {
      Logger.error(errors)

      return
    }

    const gitlab = new GitLab(config.gitlabToken)
    const github = new GitHub(config.githubToken)

    try {
      // create GitLab project
      const gitlabPathData = GitLab.parsePath(config.gitlabPath)
      const gitlabPromise = gitlab.createProject(gitlabPathData, config)

      if (config.mirror) {
        // create GitHub project
        const githubPathData = GitHub.parsePath(config.githubPath)
        const githubPromise = github.createProject(githubPathData, config)

        // wait for resolving gitlab and git project create to setup mirror
        const [gitlabRepo, githubRepo] = await Promise.all([gitlabPromise, githubPromise])

        await GitLab.setMirror(gitlabRepo, githubRepo, config)

        // is uploadCurrentDirectory set?
        if (config.uploadCurrentDir) {
          await uploadCurrentDirectory(configFile, gitlabRepo.ssh_url_to_repo)
        }
        Logger.info('Done')
      } else {
        // remove mirror
        const gitlabRepo = await gitlabPromise
        await GitLab.removeMirror(gitlabRepo, config)
      }
    } catch (error) {
      Logger.error(error)
    }
  }
}
