import path from 'path'

import test from 'ava'

import { cli } from './cli'

test('Testing cli options', async (t) => {
  // file does not exits
  t.is(await cli(['', '', '--config', './non-existing-config-file.json']), 1)

  // configuration file is a directory
  // eslint-disable-next-line unicorn/prefer-module
  t.is(await cli(['', '', '--config', path.resolve(__dirname, './models')]), 1)
})
